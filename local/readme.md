This is a tiny little docker project that allows you to build an image with a static website in.  It only works locally,
and doesn't work on OpenShift.

# Steps to use

## Clone this project

You either clone or just copy the files you need into your project.

## Copy in your static website

Put your static website in the static directory.

## Sort out your users

You need to install htpasswd locally and then create your htpasswd file.  This will get you started:

```
htpasswd -c -b htpasswd <user> <password>
```

## Build the image

```
docker build -t <tag_name> .
```

## Run to test

```
docker run -p 80:80 <tag_name>
```

You can then hit http://localhost to test.  Usually best to do this from a new window with an incognito tab.
