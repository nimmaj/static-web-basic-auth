# Small static web site docker image templates

These are a couple of small static web site, with basic auth, docker images that illustrate a
problem I was having with OpenShift dedicated.  There is a write up [here](https://ben.wyeth.me.uk/articles/openshiftDockerRootUserIssues/).

 * local is one that runs locally,
 * both is similar but runs locally _and_ on OpenShift dedicated

The username and password, in the checked in htpasswd file, are both ben.

The problem I was having was to do with the way OpenShift dedicated deals with some security issues around root users
in docker containers.  In this case I was using nginx.
